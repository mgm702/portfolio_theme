<?php

  // This is set to look and see if front page is set, if it is, the number of posts is limited to 4. if not, then it will display all posts.
  // Also the 'posts_per_page' must be filled out and in this case, the variable $num_posts is used. 
  $num_posts = (is_front_page() ) ? 4 : -1;


  $args = array(
  'post_type' => 'portfolio',
  'posts_per_page' => $num_posts
  );
  $query = new WP_Query($args);
  ?>

<section class="row no-max pad">

  <?php if ( $query->have_posts() ) : while($query->have_posts()) : $query->the_post(); ?>

  <div class="small-6 medium-4 large-3 columns grid-item">
    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
  </div>

<?php endwhile; endif; wp_reset_postdata() ?>

</section>
