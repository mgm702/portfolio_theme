<?php get_header();?>


    <section class="row">
      <div class="small-12 columns text-center">
        <div class="leader">

          <!-- This is the opening part of the content loop that states while
          having posts post the content to the screen. It is a loop that continuously
          looks for new content.-->
          <?php if ( have_posts() ) : while(have_posts() ) : the_post(); ?>
          <h1><?php the_title(); ?></h1>
          <p><?php the_content(); ?></p>
          <!--This is the PHP closing section in which the endwhile loop is used to close
          if there are no more posts. It also includes an else statement that produces
          a _e error statement that informs the user no posts available.-->
          <?php endwhile; else : ?>
          <p><?php _e('Sorry, no posts match your criteria.'); ?></p>
          <?php endif; ?>

      </div>
      </div>
    </section>




<?php get_footer();?>
